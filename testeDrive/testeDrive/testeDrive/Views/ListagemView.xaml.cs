﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testeDrive.Models;
using Xamarin.Forms;

namespace testeDrive.Views
{

    public partial class ListagemView : ContentPage
    {


        public ListagemView()
        {
            InitializeComponent();

        }

        private void ListViewVeiculos_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var veiculo = (Veiculo)e.Item; //converte objeto no veiculo

            Navigation.PushAsync(new DetalheView(veiculo)); //Navegar para outra página, pushAsync pega uma página nova e empilha na pilha de navevação.

            //DisplayAlert("Teste Drive", string.Format("Você toconou no modelo '{0}', que custa '{1}'", veiculo.Nome, veiculo.PrecoFormatado), "Ok");
        }
    }
}
