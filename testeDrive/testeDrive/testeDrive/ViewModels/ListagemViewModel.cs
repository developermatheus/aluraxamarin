﻿using System;
using System.Collections.Generic;
using System.Text;
using testeDrive.Models;

namespace testeDrive.ViewModels
{
    public class ListagemViewModel
    {
        public List<Veiculo> Veiculos { get; set; }

        public ListagemViewModel()
        {
            this.Veiculos = new ListagemVeiculos().Veiculos;
        }


    }
}
